import argparse

import numpy as np

from distances.distance import hausdorff, minimal


def _parser():
    parser = argparse.ArgumentParser(
        prog="distance_calculator",
        description="Calculate distances between points.",
    )

    parser.add_argument("x_points", type=argparse.FileType("r"))

    parser.add_argument("y_points", type=argparse.FileType("r"))

    parser.add_argument(
        "-d", "--distance", help="Distance calculation mode", default="minimal"
    )
    return parser


def main():
    parser = _parser()
    args = parser.parse_args()

    x_points = np.loadtxt(args.x_points)
    y_points = np.loadtxt(args.y_points)

    if args.distance == "minimal":
        print(minimal(x_points, y_points))
    elif args.distance == "hausdorff":
        print(hausdorff(x_points, y_points))
